/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/aphipps/gofin/cmd"

func main() {
	cmd.Execute()
}
